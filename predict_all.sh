mkdir -p ref
mkdir -p res
rm ./ref/*
rm ./res/*
python predict.py "deu$1"
python predict.py "eng$1"
python predict.py "fra$1"
python predict.py "heb$1"
python predict.py "heb_unvoc$1"
python predict.py "rus$1"
python predict.py "spa$1"
python predict.py "swa$1"
python predict.py "tur$1"
