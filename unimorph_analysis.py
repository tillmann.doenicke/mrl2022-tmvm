"""Methods to get UniMorph data.
"""

import itertools
import os
import re
import sys


# path to the "unimorph" folder
UNIMORPH_PATH = os.path.join(os.path.dirname(__file__), "unimorph")

# dictionary that maps a language to a dictionary that maps a form to tuples of lemma and morphological analysis,
# e.g. UNIMORPH_ANALYSIS["eng"]["watched"] yields {("watch", "V;PST"), ("watch", "V;V.PTCP;PST")}
UNIMORPH_ANALYSIS = {}


# fill `UNIMORPH_ANALYSIS`
for language in set(
    os.listdir(UNIMORPH_PATH) + 
    os.listdir(UNIMORPH_PATH + "_additions")
):
    if not language.startswith("."):
        UNIMORPH_ANALYSIS[language] = {}
        paths = [
            os.path.join(UNIMORPH_PATH, language, language),
            os.path.join(UNIMORPH_PATH, language, language + "_voc"),
            os.path.join(UNIMORPH_PATH + "_additions", language)
        ]
        for path in paths:
            if os.path.exists(path):
                with open(path, "r") as f:
                    for line in f:
                        if line.startswith("#"):
                            continue
                        line = line.strip().split("\t")
                        try:
                            lemma = line[0].strip()
                            form = line[1].strip()
                            feats = line[2].strip()
                        except IndexError:
                            continue
                        try:
                            UNIMORPH_ANALYSIS[language][form].add((lemma, feats))
                        except KeyError:
                            UNIMORPH_ANALYSIS[language][form] = set([(lemma, feats)])


def get_unimorph(form, language):
    """Looks up the analyses for a word.

    Args:
        form (str): The word form.
        language (str): The corresponding language.
    
    Returns:
        set of (str, str): Lemma-analysis pairs as in the `UNIMORPH_ANALYSIS` dictionary.
            For Swahili, there is a workaround based on regular expressions (see below).
    
    """
    try:
        return UNIMORPH_ANALYSIS[language][form]
    except KeyError:
        if language == "swa":
            return get_unimorph_swa(form)
        return set()



"""Methods for analyzing Swahili verbs.
The implementation is mainly based on this article: https://www.swahilicheatsheet.com/
"""

# dictionary that maps Swahili morphemes to UniMorph features
SWA_MORPHEME_DICT = {
    "Prefix" : {
        "hu" : "HAB"
    },
    "Subject" : {
        "ni" : "1;SG", "u" : "2;SG|3;SG;M_MI|3;SG;U", "a" : "3;SG|3;SG;M_WA", "tu" : "1;PL", "m" : "2;PL|IN", "wa" : "3;PL|3;PL;M_WA", "si" : "1;SG;NEG", "hu" : "2;SG;NEG", "ha" : "3;SG;NEG|3;SG;M_WA;NEG", "hatu" : "1;PL;NEG", "ham" : "2;PL;NEG|IN;NEG", "hawa" : "3;PL;NEG|3;PL;M_WA;NEG", "ku" : "NFIN|3;SG;KU|REM", "yu" : "3;SG;M_WA", "i" : "3;PL;M_MI|3;SG;N", "li" : "3;SG;JI_MA", "ya" : "3;PL;JI_MA", "ki" : "3;SG;KI_VI", "vi" : "3;PL;KI_VI", "zi" : "3;PL;N", "pa" : "PROXM", "mu" : "IN", "hayu" : "3;SG;M_WA;NEG", "hau" : "3;SG;M_MI;NEG|3;SG;U;NEG", "hai" : "3;PL;M_MI;NEG", "hali" : "3;SG;JI_MA;NEG", "haya" : "3;PL;JI_MA;NEG", "haki" : "3;SG;KI_VI;NEG", "havi" : "3;PL;KI_VI;NEG", "hazi" : "3;PL;N;NEG", "haku" : "3;SG;KU;NEG|REM;NEG", "hapa" : "PROXM;NEG", "hamu" : "IN;NEG"
    },
    "Tense" : {
        "na" : "PRS", "ta" : "FUT|FUT;NEG", "li" : "PST", "me" : "PRS;PRF", "ja" : "PRS;PRF;NEG", "ku" : "PST;NEG", "nge" : "COND;PRS|COND;PRS;NEG;LGSPEC1", "singe" : "COND;PRS;NEG", "ngali" : "COND;PST|COND;PST;NEG;LGSPEC1", "singali" : "COND;PST;NEG", "ki" : "PROG", "si" : "SUBJ;NEG|IMP;NEG"
    },
    "Object" : {
        "ni" : "1;SG", "ku" : "2;SG|3;SG;KU|REM", "m" : "3;SG|IN", "tu" : "1;PL", "wa" : "2;PL|3;PL|3;PL;M_WA", "u" : "3;SG;M_MI|3;SG;U", "a" : "3;SG;M_WA", "ha" : "3;SG;M_WA;NEG", "ham" : "IN;NEG", "hawa" : "3;PL;M_WA;NEG", "yu" : "3;SG;M_WA", "i" : "3;PL;M_MI|3;SG;N", "li" : "3;SG;JI_MA", "ya" : "3;PL;JI_MA", "ki" : "3;SG;KI_VI", "vi" : "3;PL;KI_VI", "zi" : "3;PL;N", "pa" : "PROXM", "mu" : "IN", "hayu" : "3;SG;M_WA;NEG", "hau" : "3;SG;M_MI;NEG|3;SG;U;NEG", "hai" : "3;PL;M_MI;NEG", "hali" : "3;SG;JI_MA;NEG", "haya" : "3;PL;JI_MA;NEG", "haki" : "3;SG;KI_VI;NEG", "havi" : "3;PL;KI_VI;NEG", "hazi" : "3;PL;N;NEG", "haku" : "3;SG;KU;NEG|REM;NEG", "hapa" : "PROXM;NEG", "hamu" : "IN;NEG", "ji" : "RFLX"
    }
}

# RegEx to analyze Swahili verbs (far from perfect!)
SWA_VERB_REGEX = re.compile(r'(?P<Prefix>' + "|".join(sorted(SWA_MORPHEME_DICT["Prefix"].keys(), key=len, reverse=True)) + r')?(?P<Subject>' + "|".join(sorted(SWA_MORPHEME_DICT["Subject"].keys(), key=len, reverse=True)) + r')?(?P<Tense>' + "|".join(sorted(SWA_MORPHEME_DICT["Tense"].keys(), key=len, reverse=True)) + r')?(?P<Object>' + "|".join(sorted(SWA_MORPHEME_DICT["Object"].keys(), key=len, reverse=True)) + r')?(?P<Stem>.+?[aeiou]+[^aeiou]+)(?P<Vowel>([aeiou]?[aeiou])|(([aeiou]l)?(ia|ea)))')


def get_unimorph_swa(form):
    """Computes the analyses for a Swahili word.
        (Only implemented for verbs.)

    Args:
        form (str): The word form.
    
    Returns:
        set of (str, str): Lemma-analysis pairs.
    
    """
    form_analyses = []
    lemmas = []
    match = SWA_VERB_REGEX.fullmatch(form)
    if match is not None:
        
        # get possible analyses:
        form_analyses.append(["V"])
        match_dict = match.groupdict()
        for key in ["Prefix", "Tense", "Subject", "Object"]:
            if match_dict[key] is not None:
                if match_dict[key] in SWA_MORPHEME_DICT[key] and SWA_MORPHEME_DICT[key][match_dict[key]] != "":
                    analyses = SWA_MORPHEME_DICT[key][match_dict[key]].split("|")
                    if key == "Object":
                        analyses = ["PRO;" + analysis for analysis in analyses]
                    form_analyses.append(analyses)
        
        # get possible lemmas:
        lemmas.append([match_dict["Stem"]])
        vowel = match_dict["Vowel"]
        vowels = [""]
        if vowel.endswith("ia"):
            vowel = vowel[:-2]
            vowels = ["a", "i", "u"]
        elif vowel.endswith("ea"):
            vowel = vowel[:-2]
            vowels = ["e", "o"]
        if vowel.endswith("l"):
            vowel = vowel[:-1]
        lemmas.append([vowel])
        lemmas.append(vowels)
    
    form_analyses = [";".join(analysis) for analysis in itertools.product(*form_analyses)]
    lemmas = ["".join(lemma) for lemma in itertools.product(*lemmas)]
    
    analyses = set()
    for analysis in form_analyses:
        for lemma in lemmas:
            analyses.add(((lemma, analysis)))
    
    return analyses