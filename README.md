# Multilingual Clause-Level Morphology

Contains the code for the submission to the Shared Task on Multilingual Clause-level Morphology (Task 3 Analysis).

This code was specifically designed for the shared task and it's not recommended to use it for something else. If you are interested in clause-level morphological analysis of real-world data, you may want to check out the implementation [here](https://gitlab.gwdg.de/tillmann.doenicke/disrpt2021-tmvm#tense-mood-voice-and-modality-tagging-for-11-languages).

## Set-Up

### Virtual Environment and Requirements

Clone the repository, set-up a virtual environment and install the requirements:

```sh
git clone https://gitlab.gwdg.de/tillmann.doenicke/mrl2022-tmvm.git
cd mrl2022-tmvm
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### SpaCy Models

Install pretrained [spaCy](https://spacy.io/) models:

```sh
sh spacy_models.sh
```

For the languages for which spaCy does not provide a pretrained model were trained as on the [UD treebanks](https://universaldependencies.org/) as described [here](https://github.com/explosion/projects/tree/v3/pipelines/tagger_parser_ud) and [here](https://spacy.io/usage/projects). They are saved in `resources/tagger_parser_ud/packages`.

### UniMorph Data

Download the UniMorph repos for all languages from [https://github.com/unimorph/[language]](https://github.com/unimorph/) and place them in `unimorph/[language]`.

Since this data only contains information about nouns, verbs and adjectives, we manually created additional files in `unimorph_additions` with information about pronouns, adpositions and auxiliaries.

## MRL 2022 Shared Task

### Data

Download the `MRL_shared-task_2022` repo from [https://github.com/omagolda/MRL_shared-task_2022](https://github.com/omagolda/MRL_shared-task_2022).

### Language Support

The provided scripts support the languages German (`deu`), English (`eng`), French (`fra`), (vocalized and unvocalized) Hebrew (`heb`), Russian (`rus`), Spanish (`spa`), Swahili (`swa`) and Turkish (`tur`).

If you want to run them for another language, you have to:
1. Download or train a spaCy model for the language (see above).
2. Download the UniMorph data for the language (see above). You may also want to create a file with additions. Otherwise, pronouns cannot be analyzed.
3. Fill in the language information and inflectional paradigm in `resources/inflection/lang_data.py` and `resources/inflection/table.csv`. Information about this table can be found [here](https://gitlab.gwdg.de/tillmann.doenicke/disrpt2021-tmvm/-/tree/master/#language-resources) (although it's a slightly different format).

### Prediction and Evaluation

For creating predictions, run:

```sh
python predict.py [filename]
```

The file must be located at `MRL_shared-task_2022/analysis/[filename]`. The script creates a copy in `ref/[filename]` and a result file in `res/[filename]`.

For evaluating the predictions, run:

```sh
python2 evaluate.py . . True
```

The evaluation script comes from [here](https://github.com/Fenerator/mrl_2022_shared_task_evaluation).

## License and Citation

This work is licensed under a Creative Commons Attribution 4.0 International License.

If you use this code, you should cite the following paper:

> Tillmann Dönicke (2022). "Rule-Based Clause-Level Morphology for Multiple Languages". In Proceedings of the 2nd Multilingual Representation Learning Workshop (MRL 2022).