import csv
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from analysis import Analysis


"""
`LANG_DATA` stores grammatical information for each language.
    "aux": a dictionary of auxiliary verbs that are used in composite verb forms.
        "AUX1" ... "AUX4" represent different groups of auxiliaries (for cases in which 
        A composite verb form could be built with different auxiliary verbs, depending 
        on the main verb or simply because of synonymous auxiliaries). There is no meaning
        in the number following "AUX" but I followed this rough scheme:
            - "AUX1" contains auxiliaries used to build primary compound tense-aspect forms,
                e.g. perfect forms in English
            - "AUX2" contains auxiliaries controlling voice or mood,
                e.g. passive voice in English
            - "AUX3" contains auxiliaries used to build secondary compound tense-aspect forms,
                e.g. future tense in English
            - "AUX4" contains auxiliaries functioning as copula in compound forms,
                e.g. progressive aspect in English
            - "AUX5" contains auxiliaries used in grammaticalised verbal compound constructions,
                e.g. light verbs in Persian
    "mod": a dictionary of modal verbs (those will be excluded from composite verb forms).
        The modal verbs are grouped into three categories according to their semantic function:
            - "AUX": "empty" verbs that don't have any meaning but can take inflectional features.
                e.g. "do" in English
            - "OBL": verbs associated with obligation/necessity,
                e.g. "must" and "shall" in English
            - "POS": verbs associated with possibility/ability/permission,
                e.g. "can" and "may" in English
            - "VOL": verbs associated with volition/prediction,
                e.g. "wollen" `want to` in German
    "OV": Whether the language's basic word order is object-verb (True) or verb-object (False).
        (This is order usually identical to the basic verb order: "V_subordinate-V_head".)
"""
LANG_DATA = {
    "deu" : {
        "aux" : {
            "AUX1" : ["haben", "sein"],
            "AUX2" : [],
            "AUX3" : ["werden"],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : [],
            #"POS" : [],
            #"VOL" : []
        },
        "neg" : ["nicht"],
        "OV" : True
    },
    "eng" : {
        "aux" : {
            "AUX1" : ["have"],
            "AUX2" : [],
            "AUX3" : ["will"],
            "AUX4" : ["be"],
            "AUX5" : ["would"]
        },
        "mod" : {
            "AUX" : ["do"],
            #"OBL" : ["must", "shall", "should"],
            #"POS" : ["can", "could", "may", "might"],
            #"VOL" : []
        },
        "neg" : ["not"],
        "OV" : False
    },
    "fra" : {
        "aux" : {
            "AUX1" : ["être", "avoir", "avoyr"],
            "AUX2" : [],
            "AUX3" : [],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : ["devoir"],
            #"POS" : ["pouvoir"],
            #"VOL" : ["vouloir"]
        },
        "neg" : ["ne", "pas"],
        "OV" : False
    },
    "heb" : {
        "aux" : {
            "AUX1" : [],
            "AUX2" : [],
            "AUX3" : [u"היה", u"הָיָה"],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : [],
            #"POS" : [],
            #"VOL" : []
        },
        "neg" : [u"לא", u"לֹא", u"אל", u"אַל"],
        "OV" : False
    },
    "rus" : {
        "aux" : {
            "AUX1" : ["быть"],
            "AUX2" : [],
            "AUX3" : [],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : ["приходиться"],
            #"POS" : ["мочь"],
            #"VOL" : ["хотеть", "хотеться"]
        },
        "neg" : ["не"],
        "OV" : False
    },
    "spa" : {
        "aux" : {
            "AUX1" : ["haber"],
            "AUX2" : ["estar", "ser"],
            "AUX3" : [],
            "AUX4" : ["estar"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : ["deber"],
            #"POS" : ["poder"],
            #"VOL" : []
        },
        "neg" : ["no"],
        "OV" : False
    },
    "swa" : {
        "aux" : {
            "AUX1" : ["kuwa"],
            "AUX2" : [],
            "AUX3" : [],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : [],
            #"POS" : [],
            #"VOL" : []
        },
        "neg" : [],
        "OV" : False
    },
    "tur" : {
        "aux" : {
            "AUX1" : [],
            "AUX2" : ["mi"],
            "AUX3" : ["olmak"],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            #"OBL" : [],
            #"POS" : [],
            #"VOL" : []
        },
        "neg" : [],
        "OV" : True
    }
}


"""
`LANG_TABLE` maps a language code to a list of tuples, where each tuple consists of
    1) a list of morphological analyses representing verbs, and 
    2) the grammatical anylsis of this combination.
The data is read from `table.csv`.
`EXCLUSIVE_FEATS` contains the list of features for each column (these are mutually exclusive).
"""
LANG_TABLE = {}
EXCLUSIVE_FEATS = {}
with open(os.path.join(os.path.dirname(__file__), "table.csv"), "r") as f:
    table = csv.reader(f, delimiter=',', quotechar='"')
    header = next(table)
    for row in table:
        example = row[0]
        lang = row[1]
        cols = ["VerbForm", "Mood", "Tense", "Aspect"]
        analysis = Analysis(None)
        analysis.feats = set([feat for j, col in enumerate(cols) if row[2+j] != "#" for feat in row[2+j].split(";")])
        verbs = []
        row = ["MAIN"] + row[2+len(cols):]
        for i in range(0, len(row), len(cols)+1):
            if row[i] == "":
                break
            verb = Analysis(None)
            verb.lemma = row[i]
            for j, col in enumerate(cols):
                if row[i+1+j] != "#":
                    verb.feats.add(row[i+1+j])
                    try:
                        EXCLUSIVE_FEATS[col].add(row[i+1+j])
                    except KeyError:
                        EXCLUSIVE_FEATS[col] = set([row[i+1+j]])
            if i == 0:
                verb.category = "MAIN"
            else:
                verb.category = "AUX"
            verbs.append(verb)
        compound_verb = (verbs, analysis)
        try:
            LANG_TABLE[lang].append(compound_verb)
        except KeyError:
            LANG_TABLE[lang] = [compound_verb]