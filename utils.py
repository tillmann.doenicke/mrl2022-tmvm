"""Auxiliary constants and methods used in the other scripts.
"""

import contractions
import pyinflect
import re
import unidecode

from analysis import *
from unimorph_analysis import get_unimorph


# dependency relations of NP heads
NP_RELS = set(["nsubj", "obj", "iobj", "obl", "nmod", "expl"])

# UM-style features
UM_CASES = set(
    # nominal case:
    ["NOM", "ACC", "DAT", "GEN", "ABL", "ALL", "COM", "IN", "INS", "LOC", "ON", "PRIM", "SEC"]
    # adpositional case:
    + ["ABL", "ACC", "ALL+VERS", "ANTE", "APUD", "AT", "AT+ABL", "AT+ESS", "AT+PER", "BEN", "CIRC", "COM", "CONTR", "DAT", "GEN", "IN", "IN+ABL", "IN+ALL", "IN+ESS", "IN+PER", "ON", "ON+ABL", "ON+ALL", "ON+ESS", "ONVR", "POST+ABL", "POST+ALL", "POST+ESS", "PROL", "SUB", "SUB+ABL", "SUB+ALL", "SUB+ESS", "TERM", "VERS", "VON"]
)
UM_CLASSES = set(["JI_MA", "KI_VI", "KU", "IN", "M_MI", "M_WA", "N", "PROXM", "REM", "U"])
UM_FORMAL = set(["INFM", "FORM"])
UM_GENDER = set(["MASC", "FEM", "NEUT"])
UM_NUMBER = set(["SG", "PL"])
UM_PERSON = set(["1", "2", "3"])


# Clause- and NP-level features in the order as in the output strings:
GRAMMATICAL_FEATS = ["NEC", "INFR", "IMP", "IND", "QUOT", "SBJV", "SUBJ", "COND", "LGSPEC1", "PRS", "PST", "FUT", "HAB", "PRSP", "PROG", "PERF", "PRF", "PFV", "IPFV", "IMMED", "LGSPEC2"]
NP_FEATS = ["1", "2", "3", "SG", "PL", "INFM", "FORM", "MASC", "FEM", "NEUT", "LGSPEC3", "M_WA", "M_MI", "JI_MA", "KI_VI", "N", "U", "KU", "PROXM", "REM", "IN", "RFLX"]


# The following constants and method for Hebrew preprocessing are taken from:
# https://dagshub.com/morrisalp/unikud/src/main/src/hebrew_utils.py

NIKUD_START_ORD = 1456
NIKUD_END_ORD = 1474
EXTENDED_NIKUD = {chr(i) for i in range(NIKUD_START_ORD, NIKUD_END_ORD + 1)}

def strip_nikud(s):
    if type(s) is str:
        out = s
        for N in EXTENDED_NIKUD:
            out = out.replace(N, '')
        return out
    out = s.copy() # pd Series
    for N in EXTENDED_NIKUD:
        out = out.str.replace(N, '')
    return out


def has_head_dep(token, relations):
    """Checks if a token has a specific dependency relation.
    
    Args:
        token (`Token`): A token.
        relations (iterable of str): Dependency relations.
    
    Returns:
        boolean: True iff the token is connected to its head by one of the given relations
            or by "conj" and the head has one of the given relations (subtypes are ignored).
    
    """
    dep = token.dep_.split(":")[0]
    if dep in relations:
        return True
    if dep == "conj" and token.head.dep_.split(":")[0] in relations:
        return True
    return False


def get_form(token, compound=False):
    """Return the form of a token, handle compounds and upper-/lower-casing.

    Args:
        token (`Token`): A token.
        compound (boolean): If True, compound words are connected to a single string.
    
    Returns:
        str: The token as string with usual (non sentence-initial) casing.
    
    """
    form = token.text
    if compound:
        for child in token.children:
            if child.dep_.split(":")[0] == "compound":
                if child.i < token.i:
                    form = child.text + " " + form
                else:
                    form += " " + child.text
    form = form.lower()
    if token.lemma_.isupper():
        form = form.upper()
    elif token.lemma_[0].isupper():
        form = form[0].upper() + form[1:]
    return form


def ud2um_feats(morph):
    """Converts morphological features from UD format to UM format.

    Args:
        morph (`Morphology`): The morphology object of a token.
    
    Returns:
        set of str: Set of morphological features.
    
    """
    mappings = {"Part" : "PTCP", "Past" : "PST", "Plur" : "PL", "Pres" : "PRS", "Sing" : "SG"}
    morph = morph.to_dict()
    feats = set()
    for name in morph:
        val = morph[name]
        if val in mappings:
            val = mappings[val]
        else:
            val = val.upper()
        feats.add(val)
    return feats


def get_morph_feats(token, language):
    """Return all morphological analyses of a token.

    Args:
        token (`Token`): A token.
        language (str): The corresponding language.
    
    Returns:
        list of `Analysis`: List of morphological analyses.
    
    """
    analyses = set() # result set
    forms = [] # possible forms of the token to look up
    # usually, `forms` contains only the form of the token as is, but we might add other forms, e.g.
    # if the token is a compound like a particle verb in German:
    compound = [child for child in token.subtree if child == token or (child in token.children and has_head_dep(child, ["compound"]))]
    forms.append(" ".join([child.text for child in compound]))
    if token.text != forms[0]:
        forms.append(token.text)
    
    pos = "" # UM-style part-of-speech

    for x, form in enumerate(forms):
        # analyse each form
        form_analyses = get_unimorph(form, language)

        # verbs
        if token.pos_ in ["VERB", "AUX"]:
            pos = "V"
            
            # German
            # If a German compound (presumably a particle verb) is not in the dictionary,
            # we analyse the base verb and add the particle to its lemma.
            if language == "deu" and x == 1 and len(compound) == 2:
                form_analyses = set([(compound[1].text + lemma, feats) for lemma, feats in form_analyses])
        
        # particles
        elif token.pos_ == "PART":
            pos = "PART"
        
        # pronouns
        elif token.pos_ == "PRON":
            pos = "PRO"

            # Spanish
            # If the (usually reflexive) pronoun "se" precedes "la", "las", "lo" or "los", it can have additional meanings.
            if language == "spa" and form == "se" and token.i < len(token.doc)-1 and token.doc[token.i+1].text in ["la", "las", "lo", "los"]:
                form_analyses.update(get_unimorph("le", language))
                form_analyses.update(get_unimorph("les", language))

        
        # adpositions
        elif token.pos_ == "ADP":
            pos = "ADP"
        
        # other parts of speech are not relevant for the shared task
        else:
            # raise NotImplementedError("`get_morph_feats` not implemented for token with UPOS tag: " + token.pos_)
            pass
            
        analyses.update(form_analyses)
        if len(analyses) > 0:
            # if the first form (i.e. the compound form) yields results,
            # we do not want to analyse the compound parts
            break
    
    # convert the analyses to `Analysis` objects
    analyses = [Analysis(analysis) for analysis in analyses]

    # French
    # UniMorph sometimes does not recognize participles properly.
    # So, if spaCy says its a participle and UniMorph does not, 
    # we add a participle analysis.
    if language == "fra" and pos == "V":
        um_feats = ud2um_feats(token.morph)
        if (
            len([analysis for analysis in analyses if ("PTCP" in analysis.feats and "PST" in analysis.feats)]) == 0 and 
            ("PTCP" in um_feats or len(um_feats.intersection(UM_GENDER)) > 0)
        ):
            analyses.append(Analysis(None))
            analyses[-1].lemma = token.lemma_
            analyses[-1].pos = pos
            analyses[-1].feats = set(["PTCP", "PST"]).union(um_feats)

    # if there is no analysis found,
    # we add an analysis without features
    # and with the lemma from spaCy
    if len(analyses) == 0:
        analyses = [Analysis(None)]
        analyses[0].lemma = token.lemma_
        analyses[0].pos = pos

    # English
    # Participles are often stored as nouns or adjectives and not lemmatised with the verb stem.
    # We therefore add the participle analyses if they do not already exist among the analyses.
    if language == "eng":
        analyses_with_participles = []
        for analysis in analyses:
            if ((analysis.pos == "N" and "SG" in analysis.feats) or analysis.pos == "ADJ") and analysis.lemma.endswith("ing"):
                new_analyses = [Analysis((analysis.lemma, "V.PTCP;PRS"))]
            elif ((analysis.pos == "N" and "SG" in analysis.feats) or analysis.pos == "ADJ") and analysis.lemma.endswith("ed"):
                new_analyses = [Analysis((analysis.lemma, "V.PTCP;PST")), Analysis((analysis.lemma, "V;PST"))]
            else:
                analyses_with_participles.append(analysis)
                continue
            for new_analysis in new_analyses:
                analysis_exists = False
                for old_analysis in analyses:
                    if old_analysis.feats == new_analysis.feats:
                        analysis_exists = True
                        break
                if not analysis_exists:
                    analyses_with_participles.append(new_analysis)
        analyses = analyses_with_participles

    # filter the analyses by part-of-speech type:
    analyses = filter_analyses(analyses, lambda analysis: analysis.pos == pos)
    
    return analyses


def get_NP_analyses(NP, language):
    """Return all morphological analyses of an NP.
        (Only tested for pronouns ...)

    Args:
        token (`Token`): A token, the NP's head.
        language (str): The corresponding language.
    
    Returns:
        list of `Analysis`: List of morphological analyses.
    
    """
    NP_analyses = get_morph_feats(NP, language)

    # If the NP has adpositions, replace its case feature
    # by the case feature of the adposition(s).
    child_analyses = []
    for child in NP.children:
        if child.dep_.split(":")[0] == "case":
            child_analyses.extend(get_morph_feats(child, language))
    if len(child_analyses) > 0:
        cased_NP_analyses = set()
        for NP_analysis in NP_analyses:
            for child_analysis in child_analyses:
                
                # If the NP is in prepositional case but the adposition requires an NP with another case ...
                if "ADP" in NP_analysis.feats and len(child_analysis.feats.intersection(UM_CASES)) > 1:
                    continue

                # Adpositions typically carry only one feature, namely the case
                # that they should assign to the NP. If the case to assign depends
                # on an NP's feature(s) (typically also case) then the adposition
                # must carry all these features as well. We check this here and then
                # remove the checking features, so that only the case to assign remains.
                common_feats = child_analysis.feats.intersection(NP_analysis.feats)
                if len(child_analysis.feats)-len(common_feats) <= 1:
                    child_analysis_feats = child_analysis.feats.difference(common_feats)
                    if len(child_analysis_feats) == 0:
                        child_analysis_feats = common_feats.intersection(UM_CASES)

                    cased_NP_analysis = NP_analysis.clone()
                    cased_NP_analysis.feats.difference_update(UM_CASES)
                    cased_NP_analysis.feats.update(child_analysis_feats)
                    cased_NP_analysis.feats.discard("ADP")

                    # French
                    # In the data, "eux" after "à" has no Gender feature ...
                    if language == "fra":
                        if NP.text == "eux" and child_analysis.lemma == u"à":
                            cased_NP_analysis.feats.difference_update(UM_GENDER)

                    cased_NP_analyses.add(cased_NP_analysis.hash())
        
        NP_analyses = [Analysis.unhash(cased_NP_analysis) for cased_NP_analysis in cased_NP_analyses]
    
    # If possible, remove NP analyses that are in prepositional case (because they actually
    # cannot be used without an adposition).
    NP_analyses = filter_analyses(NP_analyses, lambda NP_analysis: "ADP" not in NP_analysis.feats)
    
    return NP_analyses


def have_match(analysis1, analysis2, feats):
    """Checks whether two analyses are congruent.

    Args:
        analysis1 (`Analysis`): An analysis.
        analysis2 (`Analysis`): An analysis.
        feats (iterable of str): The features that should be congruent.
    
    Returns:
    """
    feats = set(feats)
    if len(feats) == 1:
        return feats.pop() not in analysis1.feats.symmetric_difference(analysis2.feats)
    if (
        len(analysis1.feats.intersection(feats)) == 0 or 
        len(analysis2.feats.intersection(feats)) == 0 or 
        len(analysis1.feats.intersection(analysis2.feats).intersection(feats)) > 0
    ):
        return True
    return False


def filter_analyses(analyses, filter_func, strict=False):
    """Filter a list by a filter function.
    
    Args:
        analyses (list of obj): A list to filter.
        filter_func (`func`): The filter function.
        strict (boolean): Iff False, the list is only filtered if at least one element would remain.
    
    Returns:
        list of obj: The filtered list.
    
    """
    filtered_analyses = list(filter(filter_func, analyses))
    if len(filtered_analyses) > 0 or strict:
        analyses = filtered_analyses
    return analyses


def score_analyses(analyses, score_func, return_all=False):
    """Filter a list by a score function.
    
    Args:
        analyses (list of obj): A list to filter.
        score_func (`func`): The score function.
        return_all (boolean): If True, all analyses are returned, sorted by score.
            If False, only the best scoring analyses are returned.
    
    Returns:
        list of obj: The filtered list.
    
    """
    scored_analyses = {}
    for analysis in analyses:
        s = score_func(analysis)
        try:
            scored_analyses[s].append(analysis)
        except KeyError:
            scored_analyses[s] = [analysis]
    if return_all:
        return scored_analyses
    if len(scored_analyses) > 0:
        return scored_analyses[max(scored_analyses.keys())]
    return []


def preprocess(text, language):
    """Do some language-specific preprocessing
        to obtain better results by the spaCy model.
    
    Args:
        text (str): The text to process.
        language (str): The corresponding language.
    
    Returns:
        str: The processed text.
    """
    text = text.replace("’", "'")
    if language == "eng":
        text = re.sub(r"'d ((\w+?(e|[^o]i)d|been)\W)", r" had \1", text)
        text = re.sub(r"'s ((?:(?!(?:not )?\w+ing)(?:not )?\w+)\W)", r" has \1", text)
        text = contractions.fix(text)
        text = re.sub(r" because ", r" cause ", text) # `contractions` has changed the verb "cause" to "because"
    elif language == "fra":
        text = text.replace("-t-", "-")
        text = text.replace("-", " ")
    elif language == "heb":
        text = strip_nikud(text)
    return text


def filter_rflx_agreement(clause_analysis, ordering_sensitive=False):
    """Check whether all reflexive pronouns agree with a non-reflexive NP.
        In case of agreement, the non-reflexive NP takes over features from the reflexive pronoun.

    Args:
        clause_analysis (dict of str:obj): Analysis in dict format.
        ordering_sensitive (boolean): If True, reflexive pronouns are only compared with NPs preceding them.
    
    Returns:
        boolean: Whether all reflexive pronouns agree with a non-reflexive NP.
    
    """
    total_agreement = True
    for clause_analysis_N in clause_analysis["N"]:
        if "RFLX" in clause_analysis_N["A"].feats:
            clause_analysis_NAs = [clause_analysis_N2["A"] for clause_analysis_N2 in clause_analysis["N"] if (clause_analysis_N2["T"] is None or clause_analysis_N["T"] is None or clause_analysis_N2["T"].i < clause_analysis_N["T"].i or not ordering_sensitive) and clause_analysis_N2 != clause_analysis_N and "RFLX" not in clause_analysis_N2["A"].feats]
            if clause_analysis["SA"] is not None and (clause_analysis["ST"] is None or clause_analysis_N["T"] is None or clause_analysis["ST"].i < clause_analysis_N["T"].i or not ordering_sensitive):
                clause_analysis_NAs.append(clause_analysis["SA"])
            agreement = False
            for clause_analysis_NA in clause_analysis_NAs:
                if (
                    have_match(clause_analysis_NA, clause_analysis_N["A"], UM_NUMBER) and 
                    have_match(clause_analysis_NA, clause_analysis_N["A"], UM_PERSON) and
                    have_match(clause_analysis_NA, clause_analysis_N["A"], UM_GENDER) and
                    have_match(clause_analysis_NA, clause_analysis_N["A"], UM_FORMAL)
                ):
                    agreement = True
                    clause_analysis_NA.feats.update(clause_analysis_N["A"].feats.intersection(UM_NUMBER))
            if not agreement:
                total_agreement = False
    return total_agreement


def score_rflx_subject(clause_analysis):
    """Scores how many reflexive pronouns agree with the subject.
        In case of agreement, the reflexive NP takes over features from the subject

    Args:
        clause_analysis (dict of str:obj): Analysis in dict format.
    
    Returns:
        int: Number of reflexive pronouns that agree with the subject minus total number of reflexive pronouns.
    
    """
    reflexive_pronouns = 0
    agreement_with_subject = 0
    if clause_analysis["SA"] is not None:
        for clause_analysis_N in clause_analysis["N"]:
            if "RFLX" in clause_analysis_N["A"].feats:
                reflexive_pronouns += 1
                if (
                    have_match(clause_analysis["SA"], clause_analysis_N["A"], UM_NUMBER) and 
                    have_match(clause_analysis["SA"], clause_analysis_N["A"], UM_PERSON) and
                    have_match(clause_analysis["SA"], clause_analysis_N["A"], UM_GENDER) and
                    have_match(clause_analysis["SA"], clause_analysis_N["A"], UM_FORMAL)
                ):
                    agreement_with_subject += 1
                    if len(clause_analysis_N["A"].feats.difference(UM_CASES)) == 1:
                        clause_analysis_N["A"].feats.update(clause_analysis["SA"].feats.intersection(UM_PERSON | UM_NUMBER | UM_FORMAL | UM_GENDER))
    return agreement_with_subject-reflexive_pronouns


def filter_ptcp_agreement(clause_analysis, language, compound_verb):
    """Check participle agreement (for French only).
        In case of agreement, NPs can take over features from participles.

    Args:
        clause_analysis (dict of str:obj): Analysis in dict format.
        language (str): The corresponding language.
    
    Returns:
        boolean: Whether all reflexive pronouns agree with a non-reflexive NP.
    
    """
    total_agreement = True
    if language == "fra":
        for token in compound_verb:
            if token == clause_analysis["FT"]:
                continue
            token_analyses = get_morph_feats(token, language)
            lemmas = set([token_analysis.lemma for token_analysis in token_analyses])
            if clause_analysis["GA"].lemma in lemmas:
                ptcp_analysis = Analysis(None)
                ptcp_analysis.feats = ud2um_feats(token.morph)
                if "PTCP" not in ptcp_analysis.feats.union(*[token_analysis.feats for token_analysis in token_analyses]):
                    continue
                if len(ptcp_analysis.feats.intersection(UM_GENDER)) == 0:
                    ptcp_analysis.feats.add("MASC")
                lemma = (None if clause_analysis["FA"] is None else clause_analysis["FA"].lemma)
                clause_analysis_NAs = [clause_analysis_N["A"] for clause_analysis_N in clause_analysis["N"] if "ACC" in clause_analysis_N["A"].feats and "RFLX" not in clause_analysis_N["A"].feats and clause_analysis_N["T"].i < token.i]
                if len(clause_analysis_NAs) == 0 and lemma == "être" and clause_analysis["SA"] is not None:
                    clause_analysis_NAs = [clause_analysis["SA"]]
                agreement = False
                for clause_analysis_NA in clause_analysis_NAs:
                    if (
                        have_match(clause_analysis_NA, ptcp_analysis, UM_NUMBER) and 
                        have_match(clause_analysis_NA, ptcp_analysis, UM_GENDER)
                    ):
                        agreement = True
                        clause_analysis_NA.feats.update(ptcp_analysis.feats.intersection(UM_NUMBER | UM_GENDER))
                if len(clause_analysis_NAs) > 0 and not agreement:
                    total_agreement = False
    return total_agreement


def get_ordered_clause_analysis_NP_analyses(clause_analysis):
    """Returns the analysis for each NP in the order of the NPs in the clause.

    Args:
        clause_analysis (dict of str:obj): Analysis in dict format.

    Returns:
        list of `Analysis`: Analysis for each NP in the order of the NPs in the clause,
            covert NPs are at the start of the list.
    
    """
    clause_analysis_NPAs = []
    if clause_analysis["SA"] is not None:
        clause_analysis_NPAs.append(((clause_analysis["ST"].i if clause_analysis["ST"] is not None else -1), clause_analysis["SA"]))
    for clause_analysis_N in clause_analysis["N"]:
        clause_analysis_NPAs.append(((clause_analysis_N["T"].i if clause_analysis_N["T"] is not None else -1), clause_analysis_N["A"]))
    return [clause_analysis_NPA[1] for clause_analysis_NPA in sorted(clause_analysis_NPAs, key=lambda tup: tup[0])]


def stringify_result(grammatical_analysis, NP_analyses, clausal_analysis, case_sort=[], neg_after_nom=False, q_after_nom=False, rflx_last=False):
    """Creates an output string for an analysis.

    Args:
        grammatical_analysis (dict of str:obj): Analysis in dict format.
        NP_analyses (list of `Analysis`): List of NP analyses.
        clausal_analysis (`Analysis`): Clause-level features not derived from verbs or pronouns.
        case_sort (list of str): Cases in this list are represented in the listed order.
            Cases not in the list follow in the order as in `NP_analyses`.
        neg_after_nom (boolean): If True, "NEG" is inserted after "NOM", otherwise at the end.
        q_after_nom (boolean): If True, "Q" is inserted after "NOM", otherwise at the end.
        rflx_last (boolean): If True, reflexive pronouns are represented at the end, otherwise as in `NP_analyses`.
    
    Returns:
        str: The output string.
    
    """
    clausal_analysis = clausal_analysis.clone()
    if "NEG" in grammatical_analysis.feats:
        clausal_analysis.feats.add("NEG")
        grammatical_analysis.feats.remove("NEG")
    result = grammatical_analysis.lemma + " "
    if len(grammatical_analysis.feats) > 0:
        result += ";".join(sorted(list(grammatical_analysis.feats), key=lambda feat: GRAMMATICAL_FEATS.index(feat)))
    NP_results = []
    for NP_analysis in NP_analyses:
        if len(NP_analysis.feats.intersection(NP_FEATS)) > 0:
            NP_results.append((sorted(list(NP_analysis.feats.difference(set(NP_FEATS))))+["NOM"])[0] + "(" + ",".join(sorted(list(NP_analysis.feats.intersection(set(NP_FEATS))), key=lambda feat: NP_FEATS.index(feat))) + ")")
    NP_results = sorted(NP_results, key=lambda x: case_sort.index(x.split("(")[0]) if x.split("(")[0] in case_sort else len(case_sort)+1)
    if rflx_last:
        NP_results = sorted(NP_results, key=lambda x: int("RFLX" in x))
    if len(clausal_analysis.feats) > 0:
        o = 1
        for clause_feat, feat_after_nom in [("NEG", neg_after_nom), ("Q", q_after_nom)]:
            if clause_feat in clausal_analysis.feats:
                indices = [i for i, x in enumerate(NP_results) if x.split("(")[0] == "NOM"]
                if feat_after_nom and len(indices) > 0:
                    NP_results.insert(indices[0]+o, clause_feat)
                    o += 1
                else:
                    NP_results.append(clause_feat)
    if len(NP_results) > 0:
        result += ";" + ";".join(NP_results)
    return result