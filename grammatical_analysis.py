"""Implementation of the main algorithm.
"""

import itertools

from resources.inflection.lang_data import LANG_DATA, LANG_TABLE, EXCLUSIVE_FEATS
from utils import *


def get_clause_components(clause):
    """Split a clause into components.
    
    Args:
        clause (`Doc`): A spacy object.
    
    Returns:
        list of `Token`: The compound verb form.
        list of `Token`: All NP heads.
        list of `Token`: All clause-level dependents, e.g. connectives and adverbs.

    """
    compound_verb, NPs, free_tokens = [], [], []
    for token in clause:
        if (token.pos_ in ["VERB", "AUX"]) or ((token.pos_ == "PART" or token.dep_.split(":")[0] == "compound") and token.head.pos_ in ["VERB", "AUX"]):
            compound_verb.append(token)
        elif has_head_dep(token, NP_RELS):
            NPs.append(token)
        elif token.head == clause[:].root:
            free_tokens.append(token)
    return compound_verb, NPs, free_tokens


def compute_grammatical_analyses(compound_verb, language, remove_infinite_forms=True):
    """Return (all possible) grammatical analyses of a compound verb form.
        The original algorithm and implementation is taken from https://gitlab.gwdg.de/tillmann.doenicke/disrpt2021-tmvm/-/blob/master/extract_features.py#L198
            and has been modified.

    Args:
        compound_verb (list of `Token`): The tokens of the compound verb.
        language (str): The language code.
        reduce_infinite_forms (boolean): Iff True, all analyses without a finite verb are removed.
    
    Returns:
        list of (`Token`, `Analysis`, `Analysis`): The analyses of the compound form.
            [0]: the finite verb of the construction, if existing, else None
            [1]: the morphological analysis of the finite verb, if existing, else None
            [2]: the grammatical analysis of the compound form.
    
    """
    lang_data = LANG_DATA[language]
    aux_lemmas = set([lemma for cat in lang_data["aux"] for lemma in lang_data["aux"][cat]])
    mod_lemmas = set([lemma for cat in lang_data["mod"] for lemma in lang_data["mod"][cat]])

    # get all possible morphological analyses for each token,
    # store all possible combinations of them in `compound_verbs`
    token_analyses = [get_morph_feats(token, language) for token in compound_verb]
    token_analyses_combinations = list(itertools.product(*token_analyses))
    compound_verbs = [list(zip(compound_verb, token_analyses_combination)) for token_analyses_combination in token_analyses_combinations]
    compound_verbs = [[(token, token_analysis.clone()) for token, token_analysis in compound_verb] for compound_verb in compound_verbs]

    # Turkish
    # Several forms of the auxiliary "mi" are ambiguous between SG and PL.
    # (As far as I could tell from the data:) The agreement of the subject then
    # depends on the preceding verb. So we require forms of "mi" and the preceding
    # verb to match in person and number (which effectively removes some analyses of "mi").
    if language == "tur":
        filtered_compound_verbs = []
        for compound_verb in compound_verbs:
            agreement = True
            for i, (token, token_analysis) in enumerate(compound_verb):
                if token_analysis.lemma == "mi" and i > 0:
                    verb, verb_analysis = compound_verb[i-1]
                    if not (
                        have_match(token_analysis, verb_analysis, UM_PERSON) and 
                        have_match(token_analysis, verb_analysis, UM_NUMBER)
                    ):
                        agreement = False
                        break
            if agreement:
                filtered_compound_verbs.append(compound_verb)
        if len(filtered_compound_verbs) > 0:
            compound_verbs = filtered_compound_verbs
    
    # The algorithm removes tokens from the construction in various places,
    # resulting in analyses for constructions of different length.
    # We therefore save the analyses in this dictionary, sorted by length.
    compound_analyses = {} # length of the construction : list of analyses

    for compound_verb in compound_verbs:

        # Russian
        # The word "бы" behaves more like a particle than a verb but is tagged as AUX. 
        # It further does not follow the linear syntactic ordering that is required for 
        # this algorithm. Therefore, it is converted to a particle.
        if language == "rus":
            for token, token_analysis in compound_verb:
                if token_analysis.lemma == u"бы":
                    token_analysis.pos = "PART"
                    token_analysis.feats = set(["SBJV"])

        # copy the features from particles to their head verbs; remove particles afterwards
        for token, token_analysis in compound_verb:
            if token_analysis.pos == "PART":
                for verb, verb_analysis in compound_verb:
                    if verb == token.head:
                        verb_analysis.feats.update(token_analysis.feats)
                        break
        compound_verb = [(token, token_analysis) for token, token_analysis in compound_verb if token_analysis.pos == "V"]
        
        # bring the verbs in the order of an OV language
        # basically: from syntactically lowest to syntactically highest
        if not lang_data["OV"]:
            compound_verb = list(reversed(compound_verb))
        
        # loop over finite/infinite analyses for unanalyzed verbs
        unknown_verbs = [(verb, verb_analysis) for verb, verb_analysis in compound_verb if not ("NFIN" in verb_analysis.feats or "PTCP" in verb_analysis.feats)]
        unknown_verbs_infinites = [list(x) for r in reversed(range(len(unknown_verbs)+1)) for x in itertools.combinations(unknown_verbs, r)]
        for unknown_verbs_infinite in unknown_verbs_infinites:

            # select the syntactically highest finite verb
            # remove all other finite verbs
            # counteract finite-verb movement (affects e.g. V2 languages)
            infinite_verbs = [(verb, verb_analysis) for verb, verb_analysis in compound_verb if "NFIN" in verb_analysis.feats or "PTCP" in verb_analysis.feats or (verb, verb_analysis) in unknown_verbs_infinite]
            finite_verbs = [verb for verb in compound_verb if verb not in infinite_verbs]
            if len(finite_verbs) > 1:
                finite_verbs = [finite_verbs[-1]]
            verbs = [(verb, verb_analysis.clone()) for verb, verb_analysis in infinite_verbs + finite_verbs]

            # [conjunct rules; left out in this implementation]

            if len(verbs) > 0:

                # separate the verbs into auxiliaries, modals and main verbs
                # select the syntactically highest main verb
                # cut off all lower verbs
                main_verbs = []
                for verb, verb_analysis in verbs:
                    if verb_analysis.lemma in aux_lemmas:
                        verb_analysis.category = "AUX"
                    elif verb_analysis.lemma in mod_lemmas:
                        verb_analysis.category = "MOD"
                    else:
                        verb_analysis.category = "MAIN"
                        main_verbs.append((verb, verb_analysis))
                if len(main_verbs) == 0:
                    verbs[0][1].category = "MAIN"
                    main_verbs.append(verbs[0])
                verbs = verbs[verbs.index(main_verbs[-1]):]

                # shift the features of modal verbs to syntactically lower position;
                for i in reversed(range(1, len(verbs))):
                    if verbs[i][1].category == "MOD":
                        verbs[i-1][1].feats = verbs[i][1].feats
                
                while len(verbs) > 0:
                    verbs[0][1].category = "MAIN"

                    # exclude modal verbs
                    no_mod_verbs = [(verb, verb_analysis) for verb, verb_analysis in verbs if verb_analysis.category != "MOD"]

                    # map morphological features to grammatical features
                    analyses = lookup([verb_analysis for verb, verb_analysis in no_mod_verbs], language)
                    
                    if len(analyses) > 0:
                        
                        # If any of the verbs has morphological negation,
                        # we add that to the compound analysis.
                        for verb, verb_analysis in verbs:
                            if "NEG" in verb_analysis.feats:
                                analyses_with_negation = set()
                                for analysis in analyses:
                                    analysis = Analysis.unhash(analysis)
                                    analysis.feats.add("NEG")
                                    analyses_with_negation.add(analysis.hash())
                                analyses = analyses_with_negation
                                break
                        
                        # If the main verb contains (covert) pronouns,
                        # we add them to the compound analysis.
                        analyses_with_null_objects = set()
                        for analysis in analyses:
                            analysis = Analysis.unhash(analysis)
                            analysis.pros = verbs[0][1].pros
                            analyses_with_null_objects.add(analysis.hash())
                        analyses = analyses_with_null_objects

                        finite_verb, finite_verb_analysis = (finite_verbs[-1] if len(finite_verbs) > 0 else (None, None))

                        # Hebrew
                        # The gender of verb form is not always expressed by the finite verb,
                        # but we later only check if the finite verb agrees with the subject.
                        # We therefore move the gender from any verb to the finite verb,
                        # as long as there is only one gender feature in the entire construction.
                        # (This step might be useful for other languages as well -- e.g. languages 
                        # like French which inflect participles by gender -- but is neither needed
                        # nor tested for other languages in this implementation.)
                        if language == "heb":
                            if finite_verb_analysis is not None:
                                genders = set()
                                for verb, verb_analysis in verbs:
                                    verb_genders = verb_analysis.feats.intersection(UM_GENDER)
                                    if len(genders) == 0:
                                        genders = verb_genders
                                    elif len(verb_genders) > 0:
                                        genders.intersection_update(verb_genders)
                                if len(genders) == 1:
                                    finite_verb_analysis.feats.add(genders.pop())
                        
                        # Swahili
                        # The subject agrees with the features (person, nummber ...) of the
                        # main verb (not the auxiliary verb as this algorithm assumes).
                        # We therefore move the features of the main verb to the features of
                        # the "finite" verb.
                        if language == "swa":
                            if finite_verb_analysis is not None:
                                finite_verb_analysis.feats = verbs[0][1].feats

                        # zip finite verb and grammatical analyses
                        analyses = set(zip([finite_verb] * len(analyses), [None if finite_verb is None else finite_verb_analysis.hash()] * len(analyses), analyses))

                        try:
                            compound_analyses[len(verbs)].update(analyses)
                        except KeyError:
                            compound_analyses[len(verbs)] = analyses
                        break

                    verbs = verbs[1:]
            
    if len(compound_analyses) > 0:
        # select only those analyses which involve the most tokens
        compound_analyses = compound_analyses[max(compound_analyses.keys())]
        # unpack analyses
        compound_analyses = [(finite_verb, None if finite_verb is None else Analysis.unhash(finite_verb_analysis), Analysis.unhash(analysis)) for finite_verb, finite_verb_analysis, analysis in compound_analyses]
        # remove analyses without finite verb
        if remove_infinite_forms:
            compound_analyses = filter_analyses(compound_analyses, lambda compound_analysis: compound_analysis[0] is not None)
    else:
        compound_analyses = []
    
    return compound_analyses


def is_match(analyses1, analyses2, language):
    """Checks whether two analyses lists match.
        Two analyses match iff both
            - the lemmas belong to the same class
            - the value of each morphological feature is identical

    Args:
        analyses1 (list of `Analysis`): List of analyses.
        analyses2 (list of `Analysis`): List of analyses.
        language (str): Language code.
    
    Returns:
        boolean: True iff the analyses lists match.
    
    """
    aux_verbs = LANG_DATA[language]["aux"]
    for analysis1, analysis2 in zip(analyses1, analyses2):
        if not (analysis1.lemma == analysis2.lemma or (analysis1.lemma in aux_verbs and analysis2.lemma in aux_verbs[analysis1.lemma]) or (analysis2.lemma in aux_verbs and analysis1.lemma in aux_verbs[analysis2.lemma])):
            return False
        if len(analysis1.feats) > 0 and len(analysis2.feats) > 0:
            for key in EXCLUSIVE_FEATS:
                feats1 = EXCLUSIVE_FEATS[key].intersection(analysis1.feats)
                feats2 = EXCLUSIVE_FEATS[key].intersection(analysis2.feats)
                if len(feats1.symmetric_difference(feats2)) > 0:
                    return False
    return True


def lookup(verb_analyses, language):
    """Look up a feature combination in the verb forms of a language.

    Args:
        verb_analyses (list of `Analysis`): List of morpological analyses of each verb.
        language (str): The language code.
    
    Returns:
        set of str: Stringified analyses of the feature combination.
    
    """
    main_lemma = verb_analyses[0].lemma
    verb_analyses[0].lemma = "MAIN"
    analyses = set()
    lang_table = LANG_TABLE[language]
    for verbs, analysis in lang_table:
        for combination in [verb_analyses]:#itertools.permutations(verb_analyses):
            if len(verbs) != len(combination):
                break
            elif is_match(verbs, combination, language):
                analysis = analysis.clone()
                analysis.lemma = main_lemma
                analyses.add(analysis.hash())
                break
    verb_analyses[0].lemma = main_lemma
    return analyses


def add_subjects(clause_analyses, language, NPs, required_feats=[], add_null_subject_for=None):
    """Adds subject analyses to grammatical analyses.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
        language (str): The corresponding language.
        NPs (list of `Token`): List of possible subjects (NP heads).
        required_feats (list of str): List of features that a subject must have.
        add_null_subject_for (str): Adds a `None` token as subject when a certain condition is met:
            `None`: Never.
            "imperative": If the analysis is imperative and there are language-specific rules to insert a null subject in this case.
            "flexible": If no possible subject could be found.
            "always": Alaways.
    
    Returns:
        list of (dict of str:obj): List of analyses with subjects.

    """
    clause_analyses_with_subjects = []
    languages_with_imp_rules = ["deu", "eng"] # languages with special rules for imperatives
    for clause_analysis in clause_analyses:
        subject_added = False # whether a (covert or overt) subject has been added to the current clause analysis
        
        # handle insertion of null subjects for languages with special rules for imperatives
        if "IMP" in clause_analysis["GA"].feats and add_null_subject_for is not None:
            null_subject_analysis = Analysis(None)
            # German
            if language == "deu" and (clause_analysis["FA"] is not None) and "2" in clause_analysis["FA"].feats:
                null_subject_analysis.feats = set(["2"]).union(clause_analysis["FA"].feats.intersection(UM_NUMBER))
            # English
            elif language == "eng":
                null_subject_analysis.feats = set(["2"])
            if len(null_subject_analysis.feats) > 0:
                null_subject_analysis.feats.update(set(required_feats))
                clause_analyses_with_subjects.append({
                    "FT" : clause_analysis["FT"],
                    "FA" : (None if clause_analysis["FA"] is None else clause_analysis["FA"].clone()),
                    "GA" : clause_analysis["GA"].clone(),
                    "ST" : None,
                    "SA" : null_subject_analysis
                })
                subject_added = True
                continue
        
        # select one of the (overt) NPs as subject
        for NP in NPs:
            NP_analyses = get_NP_analyses(NP, language)
            for NP_analysis in NP_analyses:
                if len(set(required_feats).difference(NP_analysis.feats)) > 0:
                    continue
                if clause_analysis["FA"] is None or (
                    have_match(NP_analysis, clause_analysis["FA"], UM_NUMBER) and
                    (have_match(NP_analysis, clause_analysis["FA"], UM_PERSON) or "FORM" in NP_analysis.feats) and
                    have_match(NP_analysis, clause_analysis["FA"], UM_GENDER)
                ):
                    subject_analysis_with_feats = NP_analysis.clone()
                    if (clause_analysis["FA"] is not None) and ("FORM" not in subject_analysis_with_feats.feats):
                        subject_analysis_with_feats.feats.update(clause_analysis["FA"].feats.intersection(UM_NUMBER | UM_GENDER))
                    clause_analyses_with_subjects.append({
                        "FT" : clause_analysis["FT"],
                        "FA" : (None if clause_analysis["FA"] is None else clause_analysis["FA"].clone()),
                        "GA" : clause_analysis["GA"].clone(),
                        "ST" : NP,
                        "SA" : subject_analysis_with_feats
                    })
                    subject_added = True
        
        # insert a null subject
        if language not in languages_with_imp_rules and (
            (add_null_subject_for == "always") or
            (add_null_subject_for == "flexible" and not subject_added)
        ):
            null_subject_analysis = Analysis(None)
            null_subject_analysis.feats.update(set(required_feats))
            if clause_analysis["FA"] is not None:
                null_subject_analysis.feats.update(clause_analysis["FA"].feats.intersection(UM_PERSON | UM_NUMBER | UM_GENDER | UM_CLASSES))
            clause_analyses_with_subjects.append({
                "FT" : clause_analysis["FT"],
                "FA" : (None if clause_analysis["FA"] is None else clause_analysis["FA"].clone()),
                "GA" : clause_analysis["GA"].clone(),
                "ST" : None,
                "SA" : null_subject_analysis
            })
            subject_added = True
    
    return clause_analyses_with_subjects


def add_other_NPs(clause_analyses, language, NPs, add_null_NP_for=None):
    """Adds analyses of non-subject NPs to grammatical analyses.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
        language (str): The corresponding language.
        NPs (list of `Token`): List of NPs (NP heads).
        add_null_NP_for (str): Adds `None` tokens for covert NPs when a certain condition is met:
            `None`: Never.
            "flexible": If no overt NP with the same features could be found.
                (Not yet implemented. Currently same as "always" and restricted to Swahili.)
            "always": Alaways.
    
    Returns:
        list of (dict of str:obj): List of analyses with NPs.

    """
    extended_clause_analyses = []
    for clause_analysis in clause_analyses:
        
        # get all combinations of analyses for the non-subject NPs
        other_NP_analyses = []
        for NP in NPs:
            if NP != clause_analysis["ST"]:
                NP_analyses = get_NP_analyses(NP, language)
                other_NP_analyses.append([NP_analysis.clone() for NP_analysis in NP_analyses])
        other_NP_analyses = list(itertools.product(*other_NP_analyses))

        # create individual grammatical analyses for all combinations
        for NP_analyses in other_NP_analyses:
            extended_clause_analyses.append({
                "FT" : clause_analysis["FT"],
                "FA" : clause_analysis["FA"],
                "GA" : clause_analysis["GA"],
                "ST" : clause_analysis["ST"],
                "SA" : clause_analysis["SA"],
                "N" : [{"T" : NP, "A" : NP_analyses[i]} for i, NP in enumerate([NP for NP in NPs if NP != clause_analysis["ST"]])],
            })

            # To add null NPs, they either need to have a case feature (e.g. "ACC")
            # among their features, or this list has to contain features that should
            # be used (every case is just used once and in the given order):
            cases = []

            # Swahili
            # Specify case names for verb-internal pronouns:
            if language == "swa":
                cases = ["PRIM", "SEC"]
            
            # add null NPs
            for NP_analysis in clause_analysis["GA"].pros:
                if (
                    (add_null_NP_for == "always") or
                    (add_null_NP_for == "flexible" and True) # Not implemented;
                    # it should be checked here whether an overt NP that matches the null NP already exists
                ):
                    NP_analysis = NP_analysis.clone()
                    has_case = (len(NP_analysis.feats.intersection(UM_CASES)) > 0)
                    if (not has_case) and len(cases) > 0:
                        NP_analysis.feats.add(cases.pop(0))
                        has_case = True
                    if has_case:
                        extended_clause_analyses[-1]["N"].append({"T" : None, "A" : NP_analysis})

    return extended_clause_analyses


def add_clause_feats(clause_analyses, language, clause):
    """Create an analysis object that stores clause-level features
        not obtained from inflectional words (e.g. verbs or pronouns), 
        e.g. features from punctuation or adverbs.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
        language (str): The corresponding language.
        clause (`Span`): The clause.
    
    Returns:
        list of (dict of str:obj): List of analyses; clause features have been added.
        `Analysis`: Analysis object that stores the clause-level non-inflectional features.
            Note that the same analysis object is added to all clause analyses,
            i.e. changing the features of this object would change it for all analyses.

    """
    clause_feats = set()
    for token in clause:
        feats = set()
        if token.lemma_ in LANG_DATA[language]["neg"]:
            clause_feats.add("NEG")
        elif token.lemma_ == "?":
            clause_feats.add("Q")
        elif token.lemma_ == "!":
            clause_feats.add("IMP")
    
    clause_feats_analysis = Analysis(None)
    clause_feats_analysis.feats = clause_feats
    for clause_analysis in clause_analyses:
        clause_analysis["CA"] = clause_feats_analysis
    
    return clause_analyses, clause_feats_analysis


def apply_fitering(clause_analyses, language, clause_feats_analysis, compound_verb, NPs):
    """Filter clause-level analyses according to grammatical constraints.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
        language (str): The corresponding language.
        clause_feats_analysis (`Analysis`): The analysis object that stores clause-level features
            not obtained from verbs or pronouns (see function `add_clause_feats`).
        compound_verb (list of `Token`): The compound verb as a list of tokens.
        NPs (list of `Token`): List of NPs (NP heads).
    
    Returns:
        list of (dict of str:obj): List of filtered analyses.

    """
    # If the clause type is imperative, the verb form also needs to be imperative:
    if "IMP" in clause_feats_analysis.feats:
        clause_feats_analysis.feats.remove("IMP")
        clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: "IMP" in clause_analysis["GA"].feats)
    else:
        clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: "IMP" not in clause_analysis["GA"].feats)
    print("FILTER_1", len(clause_analyses), clause_analyses)

    # German
    # German is the only language (in the data) where a "?" does not correspond to the "Q" feature.
    # Instead, the feature is not assigned if the clause has V2 word order. We check this here.
    if language == "deu":
        if len(compound_verb) == 0 or (len(NPs) > 0 and NPs[0].i < compound_verb[0].i):
            if "Q" in clause_feats_analysis.feats:
                clause_feats_analysis.feats.remove("Q")
                clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: "QUOT" in clause_analysis["GA"].feats)
                print("FILTER_2", len(clause_analyses), clause_analyses)
    
    # The subject should have nominative case:
    clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: clause_analysis["SA"] is None or "NOM" in clause_analysis["SA"].feats)
    print("FILTER_3", len(clause_analyses), clause_analyses)
    
    # Non-subject NPs should not have nominative case:
    clause_analyses = score_analyses(clause_analyses, lambda clause_analysis: -len([clause_analysis_N for clause_analysis_N in clause_analysis["N"] if "NOM" in clause_analysis_N["A"].feats]))
    print("FILTER_4", len(clause_analyses), clause_analyses)
    
    # The subject should not be a reflexive pronoun:
    clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: clause_analysis["SA"] is None or "RFLX" not in clause_analysis["SA"].feats)
    print("FILTER_5", len(clause_analyses), clause_analyses)

    # All reflexive pronouns should agree with a non-reflexive NP:
    clause_analyses = filter_analyses(clause_analyses, filter_rflx_agreement)
    print("FILTER_6", len(clause_analyses), clause_analyses)

    # The number of cases (morphological and adpositional) should be maximal:
    clause_analyses = score_analyses(clause_analyses, lambda clause_analysis: len((set() if clause_analysis["SA"] is None else clause_analysis["SA"].feats).union(*[clause_analysis_N["A"].feats for clause_analysis_N in clause_analysis["N"]]).intersection(UM_CASES.difference(UM_CLASSES))))
    print("FILTER_7", len(clause_analyses), clause_analyses)

    # French
    # Agreement of participles and other NPs depend on multiple factors lile auxiliary verb and word order.
    # We check this in an extra function:
    if language == "fra":
        clause_analyses = filter_analyses(clause_analyses, lambda clause_analysis: filter_ptcp_agreement(clause_analysis, language, compound_verb))
        print("FILTER_8", len(clause_analyses), clause_analyses)
    
    # If reflexive pronouns can agree with several NPs, we favour analyses in which they agree with the subject:
    clause_analyses = score_analyses(clause_analyses, score_rflx_subject)
    print("FILTER_9", len(clause_analyses), clause_analyses)

    # Hebrew
    # Data-specific post-filtering correction: If the subject is found to have
    # no person feature then we set it to second person.
    if language == "heb":
        for clause_analysis in clause_analyses:
            if clause_analysis["SA"].pos is not None and len(clause_analysis["SA"].feats.intersection(UM_PERSON)) == 0 and clause_analysis["GA"] is not None and "PRS" in clause_analysis["GA"].feats:
                clause_analysis["SA"].feats.add("2")

    # Swahili
    # Data-specific post-filtering correction: Reflexive pronouns do not have
    # other features than case.
    if language == "swa":
        for clause_analysis in clause_analyses:
            for clause_analysis_N in clause_analysis["N"]:
                if "RFLX" in clause_analysis_N["A"].feats:
                    clause_analysis_N["A"].feats.intersection_update(UM_CASES)
                    clause_analysis_N["A"].feats.add("RFLX")
    
    return clause_analyses


def combine_analyses(clause_analyses):
    """Find analyses with redundant features, remove the features and collapse the analyses.
        Example: If an NP can be all `ACC(3,PL,MASC)`, `ACC(3,PL,FEM)`, `ACC(3,PL,NEUT)`,
        these will be merged into `ACC(3,PL)`.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
    
    Returns:
        list of (dict of str:obj): List of (possibly fewer) analyses.

    """
    # find groups of analyses that are equal w.r.t.
    # - the analysis of the compound verb,
    # - the finite verb
    # - the subject
    # - the number of non-subject NPs
    combinable_analyses = {}
    for clause_analysis in clause_analyses:
        key = (clause_analysis["GA"].hash(), clause_analysis["FT"], clause_analysis["ST"], len(clause_analysis["N"]))
        if key not in combinable_analyses:
            combinable_analyses[key] = []
        combinable_analyses[key].append(clause_analysis)
    
    combined_analyses = []
    for key in combinable_analyses:
        if len(combinable_analyses[key]) == 1:
            combined_analyses.append(combinable_analyses[key][0])
        else:

            # construct an analyses where the features of all NPs are merged together
            combined_analysis = {
                "FT" : combinable_analyses[key][0]["FT"],
                "FA" : combinable_analyses[key][0]["FA"],
                "GA" : combinable_analyses[key][0]["GA"].clone(),
                "ST" : combinable_analyses[key][0]["ST"],
                "SA" : combinable_analyses[key][0]["SA"].clone(),
                "N" : [{"T" : combinable_analyses[key][0]["N"][i]["T"], "A" : combinable_analyses[key][0]["N"][i]["A"].clone()} for i in range(key[3])],
                "CA" : combinable_analyses[key][0]["CA"]
            }
            combined_analysis["SA"].feats = set().union(*[analysis["SA"].feats for analysis in combinable_analyses[key]])
            for i in range(key[3]):
                for analysis in combinable_analyses[key]:
                    combined_analysis["N"][i]["A"].feats.update(analysis["N"][i]["A"].feats)
            
            not_combineable_NPs = set() # set of indices of the NPs that are not combinable ("-1" for subject)
            
            # Check for person, number, gender and formality whether NPs can be combined. If so, remove the
            # respective features from the NPs to combine if all possible feature values occur.
            # NPs must not be combined if:
            # - some of the NPs have a feature of the category while others have not
            # - the number of features per category is not 0, 1 or the number of features in that category
            # - they differ in other features than person, number, gender and formality
            for feats in [UM_PERSON, UM_NUMBER, UM_GENDER, UM_FORMAL]:
                if (
                    (sum([max(len(analysis["SA"].feats.intersection(feats)),1) for analysis in combinable_analyses[key]]) not in [0, len(combinable_analyses[key])]) or 
                    (len(combined_analysis["SA"].feats.intersection(feats)) not in [0, 1, len(feats)]) or 
                    (len(set([frozenset(analysis["SA"].feats.difference(UM_PERSON | UM_NUMBER | UM_GENDER | UM_FORMAL)) for analysis in combinable_analyses[key]])) > 1)
                ):
                    not_combineable_NPs.add(-1)
                elif len(combined_analysis["SA"].feats.intersection(feats)) == len(feats):
                    combined_analysis["SA"].feats.difference_update(feats)
                for i in range(key[3]):
                    if (
                        (sum([max(len(analysis["N"][i]["A"].feats.intersection(feats)),1) for analysis in combinable_analyses[key]]) not in [0, len(combinable_analyses[key])]) or 
                        (len(combined_analysis["N"][i]["A"].feats.intersection(feats)) not in [0, 1, len(feats)]) or
                        (len(set([frozenset(analysis["N"][i]["A"].feats.difference(UM_PERSON | UM_NUMBER | UM_GENDER | UM_FORMAL)) for analysis in combinable_analyses[key]])) > 1)
                    ):
                        not_combineable_NPs.add(i)
                    elif len(combined_analysis["N"][i]["A"].feats.intersection(feats)) == len(feats):
                        combined_analysis["N"][i]["A"].feats.difference_update(feats)
            
            # combine all NPs that are combinable
            for analysis in combinable_analyses[key]:
                if -1 not in not_combineable_NPs:
                    analysis["SA"] = combined_analysis["SA"].clone()
                for i in range(key[3]):
                    if i not in not_combineable_NPs:
                        analysis["N"][i]["A"] = combined_analysis["N"][i]["A"].clone()
                combined_analyses.append(analysis)
    
    return combined_analyses


def final_ranking(clause_analyses, language):
    """Apply some final ranking to the found analyses.

    Args:
        clause_analyses (list of (dict of str:obj)): List of analyses in dict format.
        language (str): The corresponding language.
    
    Returns:
        list of (dict of str:obj): List of ranked analyses (best first).

    """
    lang_data = LANG_DATA[language]
    aux_lemmas = set([lemma for cat in lang_data["aux"] for lemma in lang_data["aux"][cat]])
    mod_lemmas = set([lemma for cat in lang_data["mod"] for lemma in lang_data["mod"][cat]])
    
    # down-rank analyses where the lemma is an auxiliary or modal verb
    clause_analyses = sorted(clause_analyses, key=lambda clause_analysis: int(clause_analysis["GA"].lemma in (aux_lemmas | mod_lemmas)))
    
    # MASC > FEM > NEUT > no gender
    gender_ranking = (["MASC", "FEM", "NEUT"], False)
    
    # no class > any class
    class_ranking = (list(UM_CLASSES), True)
    
    # not LGSPEC3 > LGSPEC3
    lgspec_ranking = (["LGSPEC3"], True)
    
    # NOM > ACC > DAT > other case [word order]
    case_ranking = (["NOM", "ACC", "DAT"], False)
    
    # RFLX > not RFLX (except for Spanish)
    rflx_ranking = (["RFLX"], language == "spa")
    
    # apply the rankings in the order above (least important to most important):
    for _ranking, reverse in [gender_ranking, class_ranking, lgspec_ranking, case_ranking, rflx_ranking]:
        clause_analyses = sorted(clause_analyses, key=lambda clause_analysis: tuple([_ranking.index(clause_analysis_NA.feats.intersection(_ranking).pop()) if len(clause_analysis_NA.feats.intersection(_ranking)) == 1 else len(_ranking) for clause_analysis_NA in get_ordered_clause_analysis_NP_analyses(clause_analysis)]), reverse=reverse)
    
    return clause_analyses