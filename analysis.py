"""Implementation of the `Analysis` class.
    `Analysis` objects are used to store morphological/grammatical analyes of words/clauses.
"""

class Analysis():
    
    def __init__(self, analysis, handle_prodrop=True):
        """Create an `Analysis` object with the following attributes:
            .lemma (str): Lemma.
            .pos (str): POS tag.
            .feats (set of str): Morphological features.
            .pros (list of `Analysis`): Analyses for null pronouns (if `handle_prodrop` is True).
            .category (str): This attribute is "" at the beginning and for verbs later changed to their type ("MAIN", "AUX", "MOD").

        Args:
            analysis ((str,str)): Tuple with
                [0] lemma
                [1] (semicolon-separated) unimorph string, 
                    starting with the POS tag
            handle_prodrop (boolean): If the unimorph string
                contains non-initial "PRO"s, the following features
                are treated as features of a dropped pronoun 
                instead of the word.
        
        """
        if analysis is None:
            self.lemma = ""
            self.pos = ""
            self.feats = set()
            self.pros = []
        else:
            feats = analysis[1].replace(".", ";").split(";")
            if handle_prodrop:
                feat_groups = []
                for feat in feats:
                    if len(feat_groups) == 0 or feat == "PRO":
                        feat_groups.append([feat])
                    else:
                        feat_groups[-1].append(feat)
            else:
                feat_groups = [feats]
            self.lemma = analysis[0]
            self.pos = feat_groups[0][0]
            self.feats = set(feat_groups[0][1:])
            self.pros = [Analysis(("PRO", ";".join(feat_group)), False) for feat_group in feat_groups[1:]]
        self.feats.discard(self.pos)
        self.category = ""
    
    def __str__(self):
        return str((self.lemma, self.pos, ";".join(sorted(list(self.feats)))))
    
    def __repr__(self):
        return self.__str__()
    
    def hash(self):
        """Return a hashable representation of the object.
        """
        return (self.lemma, self.pos, frozenset(self.feats), tuple([analysis.hash() for analysis in self.pros]), self.category)
    
    def unhash(tup):
        """Return an object from a hash (inverse of `hash`).
        """
        analysis = Analysis(None)
        analysis.lemma = tup[0]
        analysis.pos = tup[1]
        analysis.feats = set(tup[2])
        analysis.pros = [Analysis.unhash(analysis) for analysis in tup[3]]
        analysis.category = tup[4]
        return analysis
    
    def clone(self):
        """Clone the analysis.
        """
        return Analysis.unhash(self.hash())