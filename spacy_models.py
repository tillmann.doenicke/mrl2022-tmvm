"""Models and code for spaCy.
"""

import json
import os
import spacy
import string
import sys
from spacy.language import Language
from spacy.tokens import Doc

from analysis import *
from resources.inflection.lang_data import LANG_DATA
from unimorph_analysis import get_unimorph
from utils import *


# path to trained spaCy models
SPACY_MODELS_PATH = os.path.join(os.path.dirname(__file__), "resources", "tagger_parser_ud", "packages")

# spacy models for all languages
SPACY_MODELS = {
    "deu" : spacy.load(os.path.join(SPACY_MODELS_PATH, "de_ud_de_hdt-0.0.0", "de_ud_de_hdt", "de_ud_de_hdt-0.0.0")),
    "eng" : spacy.load(os.path.join(SPACY_MODELS_PATH, "en_ud_en_gum-0.0.0", "en_ud_en_gum", "en_ud_en_gum-0.0.0")),
    "fra" : spacy.load("fr_core_news_lg"),
    "heb" : spacy.load(os.path.join(SPACY_MODELS_PATH, "he_ud_he_iahltwiki-0.0.0", "he_ud_he_iahltwiki", "he_ud_he_iahltwiki-0.0.0")),
    "rus" : spacy.load("ru_core_news_lg"),
    "spa" : spacy.load("es_core_news_lg"),
    "tur" : spacy.load(os.path.join(SPACY_MODELS_PATH, "tr_ud_tr_kenet-0.0.0", "tr_ud_tr_kenet", "tr_ud_tr_kenet-0.0.0")),
}


@Language.component("punct_fixer")
def punct_fixer(doc):
    """A spaCy component that
        - fixes some frequent parsing errors related to punctuation
    
    Args:
        doc (`Doc`): The document to parse.
    
    Returns:
        `Doc`: The updated document.
    
    """
    print("FIXER")
    for token in doc:
        print(token.i, token.pos_, token.dep_, token.head.i, token.lemma_, token.morph)
    
    punctuation = string.punctuation.replace("'", "")
    new_doc = Doc(doc.vocab, [(token.text.strip(punctuation) if token.text.strip(punctuation) != "" else token.text) for token in doc], [(token.whitespace_ != "") for token in doc])
    for token, new_token in zip(doc, new_doc):
        new_token.lemma_ = token.lemma_
        new_token.pos_ = token.pos_
        new_token.morph = token.morph
        new_token.dep_ = token.dep_
        new_token.head = new_doc[token.head.i]
        if token.text.strip(punctuation) == "":
            new_token.pos_ = "PUNCT"
            new_token.dep_ = "punct"
    return new_doc


def unimorph_fixer(doc, language):
    """A spaCy component that
        - fixes some frequent parsing errors and
        - assimilates spaCy's part-of-speech tags and lemmas with those from Unimorph.
    
    Args:
        doc (`Doc`): The document to parse.
            Assumes that the document is a single clause!
        language (str): The corresponding language.
    
    Returns:
        `Doc`: The updated document.
    
    """
    lang_data = LANG_DATA[language]
    aux_lemmas = set([lemma for cat in lang_data["aux"] for lemma in lang_data["aux"][cat]])
    mod_lemmas = set([lemma for cat in lang_data["mod"] for lemma in lang_data["mod"][cat]])
    aux_lemmas.update(mod_lemmas)
    neg_lemmas = set(lang_data["neg"])
    
    verb_count = 0 # number of main verbs
    verb = None # last main verb
    
    for token in doc:

        if token.pos_ == "PUNCT":
            # nothing to change for punctuation
            continue
        if token.lemma_ in neg_lemmas:
            # negation words are set to adverbs
            # (sorry for languages in which they are particles or something else)
            token.pos_ = "ADV"
            token.dep_ = "advmod"
            continue

        # analyse the token's form
        analyses = get_unimorph(token.text, language)
        
        # English
        # Participles are often stored as nouns or adjectives.
        # Nouns and adjectives ending with "-ing" and "-ed" are therefore converted to verbs.
        # Note that this only works if there are no nouns in the input.
        if language == "eng":
            new_analyses = set()
            for lemma_analysis in analyses:
                lemma, analysis = lemma_analysis
                if analysis in ["N;SG", "ADJ"] and (lemma.endswith("ing") or lemma.endswith("ed")):
                    analysis = "V"
                new_analyses.add((lemma, analysis))
            analyses = new_analyses
        
        # French
        # Participles are often stored as nouns or adjectives.
        # The verb analysis is therefore added for nouns and adjectives.
        if language == "fra":
            if len(analyses) == 0 and token.pos_ in ["ADJ", "NOUN"]:
                analyses.add((token.lemma_, "V"))

        # convert the analyses to `Analysis` objects
        analyses = [Analysis(analysis) for analysis in analyses]
        
        # get the part-of-speech tags from all analyses
        poss = set([analysis.pos for analysis in analyses])
        
        # In few cases, a preposition and a pronoun have the same form
        # (e.g. "en" in French). If the token is the child of an NP,
        # we favour the adposition analysis.
        if "PRO" in poss and "ADP" in poss:
            if has_head_dep(token, NP_RELS):
                poss.remove("ADP")
            else:
                poss.remove("PRO")

        # reduce the analyses to pronouns/adpositions/verbs (whatever is possible first)
        for pos in ["PRO", "ADP", "V"]:
            if pos in poss:
                analyses = [analysis for analysis in analyses if analysis.pos == pos]
                poss = set([pos])
                break
        
        # Swahili
        # If no analysis is found, we interpret it as verb.
        if language == "swa":
            if len(poss) == 0:
                poss.add("V")
        
        # get the lemmas from all analyses
        lemmas = set([analysis.lemma for analysis in analyses])
        
        # If there is only one part-of-speech tag among the analyses,
        # set spaCy's part-of-speech tag and dependency relation accordingly.
        if len(poss) == 1:
            pos = poss.pop()
            if pos == "ADP":
                token.pos_ = "ADP"
                token.dep_ = "case"
            elif pos == "N":
                token.pos_ = "NOUN"
                if not has_head_dep(token, NP_RELS):
                    token.dep_ = "obj"
            elif pos == "PART":
                token.dep_ = "compound:prt"
            elif pos == "PRO":
                token.pos_ = "PRON"
                if not has_head_dep(token, NP_RELS):
                    token.dep_ = "obj"
            elif pos == "V":
                if len(lemmas.intersection(aux_lemmas)) > 0:
                    token.pos_ = "AUX"
                    token.dep_ = "aux"
                else:
                    token.pos_ = "VERB"
                    if has_head_dep(token, NP_RELS):
                        token.dep_ = "dep"
        
        # If there is only one lemma among the analyses,
        # overwrite spaCy's lemma with that.
        if len(lemmas) == 1:
            lemma = lemmas.pop()
            token.lemma_ = lemma
        
        # update the main verb (counter)
        if token.pos_ == "VERB":
            verb_count += 1
            verb = token
    
    # If there is only one main verb in the document, then this is the root.
    # We set its head relation to "ROOT" and all nominals and subordinate verbs and verbal particles as its children.
    if verb_count == 1:
        for token in doc:
            if token == verb:
                token.dep_ = "ROOT"
                token.head = token
            elif len(set([token.dep_, token.dep_.split(":")[0]]).intersection(NP_RELS.union(["aux", "compound:prt"]))) > 0:
                token.head = verb
            #elif token.dep_ == "ROOT":
            elif token.head == token:
                token.head = verb
                if token.pos_ in ["NOUN", "PRON"]:
                    token.dep_ = "obj"
                elif token.pos_ == "AUX":
                    token.dep_ = "aux"
                else:
                    token.dep_ = "dep"
                token.head = verb
    
    # Adpositions that do not have an NP as head get a new head:
    # - the preceding token if it is an NP and the language is OV
    # - the succeeding token if it is an NP and the language us VO
    # - any of the neighbouring tokens if only one is an NP
    for token in doc:
        if token.dep_.split(":")[0] == "case" and not has_head_dep(token.head, NP_RELS):
            possible_heads = []
            if token.i > 0 and has_head_dep(doc[token.i-1], NP_RELS):
                possible_heads.append(-1)
            if token.i < len(doc)-1 and has_head_dep(doc[token.i+1], NP_RELS):
                possible_heads.append(1)
            if len(possible_heads) == 1:
                token.head = doc[token.i+possible_heads[0]]
            elif len(possible_heads) == 2:
                token.head = doc[token.i+possible_heads[int(not lang_data["OV"])]]
    
    return doc


@Language.component("unimorph_fixer_deu")
def unimorph_fixer_deu(doc):
    return unimorph_fixer(doc, "deu")

@Language.component("unimorph_fixer_eng")
def unimorph_fixer_eng(doc):
    return unimorph_fixer(doc, "eng")

@Language.component("unimorph_fixer_fra")
def unimorph_fixer_fra(doc):
    return unimorph_fixer(doc, "fra")

@Language.component("unimorph_fixer_heb")
def unimorph_fixer_heb(doc):
    return unimorph_fixer(doc, "heb")

@Language.component("unimorph_fixer_rus")
def unimorph_fixer_rus(doc):
    return unimorph_fixer(doc, "rus")

@Language.component("unimorph_fixer_spa")
def unimorph_fixer_spa(doc):
    return unimorph_fixer(doc, "spa")

@Language.component("unimorph_fixer_swa")
def unimorph_fixer_swa(doc):
    return unimorph_fixer(doc, "swa")

@Language.component("unimorph_fixer_tur")
def unimorph_fixer_tur(doc):
    return unimorph_fixer(doc, "tur")


# add the custom components to the pipeline
for lang in SPACY_MODELS.keys():
    SPACY_MODELS[lang].add_pipe("punct_fixer")
    SPACY_MODELS[lang].add_pipe("unimorph_fixer_" + lang)
