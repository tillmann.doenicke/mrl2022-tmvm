"""Script to call from command line.
    The first and only argument is the name of the file (without path) to make predictions for, e.g. "eng.dev".
    The path to the file is added automatically.
"""

import nltk
import numpy as np
import os
import shutil
import sys
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from spacy.tokens import Doc
from spacy.vocab import Vocab
from spellchecker import SpellChecker

from analysis import *
from grammatical_analysis import *
from spacy_models import *
from utils import *

nltk.download('omw-1.4')


# path to the "MRL_shared-task_2022/analysis" folder
IN_PATH = os.path.join(os.path.dirname(__file__), "MRL_shared-task_2022", "analysis")

# path to the folder that contains "ref" and "res"
EVAL_PATH = os.path.join(os.path.dirname(__file__))


# Languages with case-sorting in the string representation:
CASE_SORT = {
    "fra" : ["NOM", "COM", "BEN", "CONTR", "IN", "ON", "PROL", "SUB", "VERS", "ACC", "DAT", "GEN", "LOC"],
    "spa" : ["NOM", "ACC", "DAT", "GEN", "ALL", "BEN", "COM", "CONTR", "LOC", "PER", "INTER", "ON", "TERM", "VERS"]
}

# Languages where the "NEG" feature is listed after the subject in the string representation:
NEG_AFTER_NOM = ["eng", "heb", "rus", "spa", "tur"]

# Languages where the "Q" feature is listed after the subject in the string representation:
Q_AFTER_NOM = ["eng", "rus", "spa", "tur"]

# Languages where "RFLX" pronouns are listed at the end of the string representation:
RFLX_LAST = ["fra"]


# Turn on/off print statements for more insights.
SHOW_PRINTS = False


def predict_analyses(text, language):
    """Predict analyses for an input texr.

    Args:
        text (str): The input text (a clause).
        language (str): The corresponding language.
    
    Returns:
        list of str: List of possible outputs, ranked (best first).
    
    """
    # save words for Hebrew and Swahili workaround
    words = word_tokenize(text)

    # preprocess text
    text = preprocess(text, language)
    
    # Swahili
    # There is no spaCy model (or data to train one) available,
    # so we just create a `Doc` object and add the tokens:
    if language == "swa":
        clause = Doc(Vocab(strings=words), words)
        for token in clause:
            token.lemma_ = token.text
        clause = punct_fixer(clause)
        clause = unimorph_fixer(clause, "swa")
    
    # Otherwise, parse text with spaCy:
    else:
        nlp = SPACY_MODELS[language]
        clause = nlp(text)

    # Hebrew
    # We have only a spaCy model for unvocalized Hebrew and want to use this model for both
    # unvocalized and vocalized Hebrew. We therefore convert every Hebrew input to unvocalized
    # Hebrew for parsing. After parsing, we copy the result to a new spaCy object with
    # the original token texts.
    if language == "heb":
        if len(words) == len(clause):
            new_clause = Doc(clause.vocab, words, [(token.whitespace_ != "") for token in clause])
            for token, new_token in zip(clause, new_clause):
                new_token.lemma_ = token.lemma_
                new_token.pos_ = token.pos_
                new_token.dep_ = token.dep_
                new_token.head = new_clause[token.head.i]
            clause = new_clause
            clause = unimorph_fixer(clause, "heb")

    print("PARSE")
    for token in clause:
        print(token.i, token.pos_, token.dep_, token.head.i, token.lemma_, token.morph, get_morph_feats(token, language))
    
    # split clause into compund verb, NP heads and other tokens
    compound_verb, NPs, others = get_clause_components(clause)
    print("COMPOUND_VERBS/NPs/OTHER", compound_verb, NPs, others)
    
    # perform grammatical analysis
    grammatical_analyses = compute_grammatical_analyses(compound_verb, language)

    # if no verb was found in the clause, we add a dummy analysis
    # so that at least the pronouns can be analysed:
    if len(grammatical_analyses) == 0:
        grammatical_analyses = [(None, None, Analysis(None))]
    
    # convert grammatical analyses into dictionary format
    # key (including keys added in the future):
    #   "FT": finite verb (`Token` or `None`)
    #   "FA": finite verb (`Analysis` or `None`)
    #   "GA": compound verb (`Analysis`)
    #   "ST": subject (`Token` or `None`)
    #   "SA": subject (`Analysis` or `None`)
    #   "CA": clause (`Analysis`)
    #   "N": [{
    #       "T": non-subject nominal (`Token`)
    #       "A": non-subject nominal (`Analysis`)
    #   }]
    clause_analyses = [
        {
            "FT" : finite_verb, 
            "FA" : finite_verb_analysis, 
            "GA" : grammatical_analysis
        }
        for finite_verb, finite_verb_analysis, grammatical_analysis in grammatical_analyses
    ]
    print("ANALYSES_1", len(clause_analyses), clause_analyses)

    # add subjects and other NPs
    clause_analyses = add_subjects(clause_analyses, language, NPs, ["NOM"], add_null_subject_for="flexible")
    print("ANALYSES_2", len(clause_analyses), clause_analyses)
    clause_analyses = add_other_NPs(clause_analyses, language, NPs, add_null_NP_for="flexible")
    print("ANALYSES_3", len(clause_analyses), clause_analyses)

    # add clausal features (like "Q")
    clause_analyses, clause_feats_analysis = add_clause_feats(clause_analyses, language, clause)

    # filter analyses
    clause_analyses = apply_fitering(clause_analyses, language, clause_feats_analysis, compound_verb, NPs)

    # merge analyses
    clause_analyses = combine_analyses(clause_analyses)
    
    # rank analyses
    clause_analyses = final_ranking(clause_analyses, language)

    # English
    # Sometimes, UNIMORPH contains incorrect lemmas with a trailing "e"
    # (e.g. `answere` instead of `answer`); we fix this here:
    if language == "eng":
        wnl = WordNetLemmatizer()
        sc = SpellChecker()
        for clause_analysis in clause_analyses:
            new_lemma = wnl.lemmatize(clause_analysis["GA"].lemma, "v")
            if new_lemma.endswith("e") and new_lemma[:-1] in sc.candidates(new_lemma):
                new_lemma = new_lemma[:-1]
            clause_analysis["GA"].lemma = new_lemma

    # convert the analyses to output strings:
    clause_analyses_strings = []
    for clause_analysis in clause_analyses:
        clause_analyses_string = stringify_clause_analysis(clause_analysis, language, NPs)
        if clause_analyses_string not in clause_analyses_strings:
            clause_analyses_strings.append(clause_analyses_string)
    
    return clause_analyses_strings


def stringify_clause_analysis(clause_analysis, language, NPs):
    """Get string representation of an analysis.

    Args:
        clause_analysis (dict of str:obj): Analysis in the dict format.
        language (str): The corresponding language.
        NPs (list of `Token`): List of NPs (NP heads).
    
    Returns:
        str: String in output format.
    
    """
    # Store the analyses for NPs in `clause_analysis_N`;
    # insert null analysis of null subject at the start:
    clause_analysis_N = [clause_analysis_N["A"] for clause_analysis_N in clause_analysis["N"]]
    subj_index = None
    if clause_analysis["ST"] is not None:
        subj_index = NPs.index(clause_analysis["ST"])
    elif clause_analysis["SA"] is not None:
        subj_index = 0
    if subj_index is not None:
        clause_analysis_N.insert(subj_index, clause_analysis["SA"])
    
    # get string representation
    result = stringify_result(clause_analysis["GA"], clause_analysis_N, clause_analysis["CA"], case_sort=(CASE_SORT[language] if language in CASE_SORT else []), neg_after_nom=(language in NEG_AFTER_NOM), q_after_nom=(language in Q_AFTER_NOM), rflx_last=(language in RFLX_LAST))
    
    # "LGSPEC1" comes directly before "NOM" in all languages but Turkish
    if language != "tur" and ";LGSPEC1" in result:
        result = result.replace(";LGSPEC1", "").replace(";NOM(", ";LGSPEC1;NOM(", 1)
    
    # all Swahili analyses start with a "V" feature
    if language == "swa":
        result = result.replace(" ", " V;")
    
    return result


if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        filename = args[1]
        language = ".".join(filename.split(".")[:-1]).replace("_covered", "")
        language = language.split("_")[0] # heb_unvoc -> heb
        shutil.copyfile(os.path.join(IN_PATH, filename), os.path.join(EVAL_PATH, "ref", filename))
        with open(os.path.join(EVAL_PATH, "ref", filename), "r") as f_in:
            with open(os.path.join(EVAL_PATH, "res", filename), "w") as f_out:

                # disable printing
                if not SHOW_PRINTS:
                    sys_stdout = sys.stdout
                    sys.stdout = open(os.devnull, 'w')
                
                # iterate over file
                analyses_per_line = []
                for line in f_in:
                    print(line.strip())
                    line = line.strip().split("\t")
                    text = line[0]

                    # get gold analysis (if existing)
                    gold_analysis = (line[1] if len(line) > 1 else None)

                    # get predicted analyses
                    pred_analyses = predict_analyses(text, language)

                    analyses_per_line.append(len(pred_analyses))

                    if gold_analysis is not None:
                        print("G:", gold_analysis)
                    for pred_analysis in pred_analyses:
                        print("P:", pred_analysis)
                    
                    # write result to file
                    result = (pred_analyses[0] if len(pred_analyses) > 0 else "")
                    out_line = text + "\t" + result + "\n"
                    f_out.write(out_line)
                    
                    print()
                
                # average number of analyses per input, standard deviation
                print(np.mean(analyses_per_line), np.std(analyses_per_line))
                
                # enable printing
                if not SHOW_PRINTS:
                    sys.stdout = sys_stdout
    
    else:
        raise Exception("You must specify a file.")